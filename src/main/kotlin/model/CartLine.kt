package model

import roundToHundrethDecimal

class CartLine (val product: Product, val quantity: Int) {
    var taxes: Double = 0.0

    private val price: Double
        get() = product.price * quantity

    val taxedPrice: Double
        get() = (price + taxes).roundToHundrethDecimal()
}
