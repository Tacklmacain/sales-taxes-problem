package model

class ImportedProductCondition: Condition {
    override fun isTaxable(product: Product) = product.isImported
}