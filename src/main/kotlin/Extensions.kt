fun Double.roundToHundrethDecimal() = kotlin.math.round(this * 100.0) / 100.0

fun Double.roundUpToTwentiethDecimal() = kotlin.math.ceil(this * 20.0) / 20.0
