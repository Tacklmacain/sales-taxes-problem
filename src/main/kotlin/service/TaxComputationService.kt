package service

import model.Product
import model.Tax

interface TaxComputationService {
    fun computeTaxes(taxes: List<Tax>, product: Product, quantity: Int): Double
}