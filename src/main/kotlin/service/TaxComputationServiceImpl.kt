package service

import model.*
import roundUpToTwentiethDecimal

class TaxComputationServiceImpl: TaxComputationService {

    override fun computeTaxes(taxes: List<Tax>, product: Product, quantity: Int) = taxes.sumByDouble { getTax(it, product, quantity) }

    private fun getTax(tax: Tax, product: Product, quantity: Int): Double {
        return if(tax.condition.isTaxable(product))
            ((product.price * tax.value) / 100).roundUpToTwentiethDecimal() * quantity
        else
            0.0
    }
}