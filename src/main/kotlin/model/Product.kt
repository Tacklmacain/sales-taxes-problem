package model

data class Product (
    val name: String,
    val price: Double,
    val type: ProductType,
    val isImported: Boolean
)
