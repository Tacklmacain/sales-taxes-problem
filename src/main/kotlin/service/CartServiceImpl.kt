package service

import model.Cart
import repository.TaxRepository

class CartServiceImpl(
    private val taxComputationService: TaxComputationService,
    private val taxRepository: TaxRepository
): CartService {

    override fun getCartForCheckout(cart: Cart): Cart {
        if(cart.cartLines.isEmpty())
            return cart

        val taxes = taxRepository.getAll()

        cart.cartLines.forEach { it.taxes = taxComputationService.computeTaxes(taxes, it.product, it.quantity) }

        return cart
    }
}