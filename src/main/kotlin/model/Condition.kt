package model

interface Condition {
    fun isTaxable(product: Product): Boolean
}
