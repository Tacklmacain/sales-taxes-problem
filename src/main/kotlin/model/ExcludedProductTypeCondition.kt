package model

class ExcludedProductTypeCondition(private val productsExcluded: List<ProductType>): Condition {
    override fun isTaxable(product: Product) = !productsExcluded.contains(product.type)
}