# Sales Taxes Problem

Sales Taxes Problem is a small programming problem by xpeppers, more info can be found  [clicking here](https://github.com/xpeppers/sales-taxes-problem).

## Getting Started
These instructions will get you a copy of the project up and running on your local machine.

### Installation
The project can be run using almost any IDE such as IntelliJ or Eclipse, the following steps will show how to run it on UNIX terminal.

The program is built using the package manager [gradle](https://gradle.org), it can be installed using the instruction [in this page](https://gradle.org/install/).

After installing gradle, proceed to clone this repository running:
```bash
git clone https://gitlab.com/Tacklmacain/sales-taxes-problem.git
```
After the clone move the current working directory on the cloned one:
```bash
cd sales-taxes-problem
```
Then create the gradle wrapper
```bash
gradle wrapper
```
Lastly, run the tests:
```bash
./gradlew test
```
You should see the tests results:
```
SalesTaxesTests

  Test testCartWithBothImportedAndNotImported() PASSED
  Test testCartAllImported() PASSED
  Test testCartWithoutImports() PASSED
```

## Built with
- [Kotlin](https://kotlinlang.org): The programming language chosen
- [Junit](https://junit.org/junit5/): Testing framework
- [Gradle](https://gradle.org): Package manager

## License
This project is licensed under the MIT License - see the LICENSE.md file for details
