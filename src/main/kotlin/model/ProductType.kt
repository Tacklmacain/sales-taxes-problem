package model

enum class ProductType { Book, Medical, Food, Music, Cosmetic }