package repository

import model.Tax

interface TaxRepository {
    fun getAll(): List<Tax>
}