package model

import roundToHundrethDecimal

class Cart (val cartLines: List<CartLine>) {
    val total: Double
        get() = cartLines.sumByDouble { it.taxedPrice }

    val taxes: Double
        get() = (cartLines.sumByDouble { it.taxes }).roundToHundrethDecimal()
}