import model.*
import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.*
import repository.TaxRepository
import service.CartServiceImpl
import service.TaxComputationServiceImpl

class SalesTaxesTests {

    private val taxRepository = object: TaxRepository {
        override fun getAll(): List<Tax> {
            return listOf(
                Tax(
                    "NotFoodMedicalOrBookGood",
                    10.0,
                    ExcludedProductTypeCondition(
                        listOf(
                            ProductType.Food,
                            ProductType.Book,
                            ProductType.Medical
                        )
                    )
                ),
                Tax("ImportedGood", 5.0, ImportedProductCondition())
            )
        }
    }

    private val taxService = TaxComputationServiceImpl()

    private val cartService = CartServiceImpl(taxService, taxRepository)

    @Test
    fun testCartWithoutImports() {
        val cart = Cart(
            listOf(
                CartLine(
                    Product("book", 12.49, ProductType.Book, isImported = false),
                    quantity = 2
                ),
                CartLine(
                    Product("music CD", 14.99, ProductType.Music, isImported = false),
                    quantity = 1
                ),
                CartLine(
                    Product("chocholate bar", 0.85, ProductType.Food, isImported = false),
                    quantity = 1
                )
            )
        )

        val checkoutCart = cartService.getCartForCheckout(cart)

        assertThat(checkoutCart.cartLines[0].taxedPrice).isEqualTo(24.98)
        assertThat(checkoutCart.cartLines[1].taxedPrice).isEqualTo(16.49)
        assertThat(checkoutCart.cartLines[2].taxedPrice).isEqualTo(0.85)

        assertThat(checkoutCart.taxes).isEqualTo(1.50)
        assertThat(checkoutCart.total).isEqualTo(42.32)
    }

    @Test
    fun testCartAllImported() {
        val cart = Cart(
            listOf(
                CartLine(
                    Product("imported box of chocolates", 10.00, ProductType.Food, isImported = true),
                    quantity = 1
                ),
                CartLine(
                    Product("imported bottle of perfume", 47.50, ProductType.Cosmetic, isImported = true),
                    quantity = 1
                )
            )
        )

        val checkoutCart = cartService.getCartForCheckout(cart)

        assertThat(checkoutCart.cartLines[0].taxedPrice).isEqualTo(10.50)
        assertThat(checkoutCart.cartLines[1].taxedPrice).isEqualTo(54.65)

        assertThat(checkoutCart.taxes).isEqualTo(7.65)
        assertThat(checkoutCart.total).isEqualTo(65.15)
    }

    @Test
    fun testCartWithBothImportedAndNotImported() {
        val cart = Cart(
            listOf(
                CartLine(
                    Product("imported bottle of perfume", 27.99, ProductType.Cosmetic, isImported = true),
                    quantity = 1
                ),
                CartLine(
                    Product("bottle of perfume", 18.99, ProductType.Cosmetic, isImported = false),
                    quantity = 1
                ),
                CartLine(
                    Product("packet of headache pills", 9.75, ProductType.Medical, isImported = false),
                    quantity = 1
                ),
                CartLine(
                    Product("box of imported chocolates", 11.25, ProductType.Food, isImported = true),
                    quantity = 3
                )
            )
        )

        val checkoutCart = cartService.getCartForCheckout(cart)

        assertThat(checkoutCart.cartLines[0].taxedPrice).isEqualTo(32.19)
        assertThat(checkoutCart.cartLines[1].taxedPrice).isEqualTo(20.89)
        assertThat(checkoutCart.cartLines[2].taxedPrice).isEqualTo(9.75)
        assertThat(checkoutCart.cartLines[3].taxedPrice).isEqualTo(35.55)

        assertThat(checkoutCart.taxes).isEqualTo(7.90)
        assertThat(checkoutCart.total).isEqualTo(98.38)
    }
}