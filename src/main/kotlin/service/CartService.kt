package service
import model.Cart

interface CartService {
    fun getCartForCheckout(cart: Cart): Cart
}