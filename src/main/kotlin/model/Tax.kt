package model

data class Tax (
    val name: String,
    val value: Double,
    val condition: Condition
)